﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCloud.Common
{
    public class Dictionary
    {

        public class FreezerWebAPI
        {
            public const string RegisterFreezer = "RegisterFreezer";
            public const string CreateAndAssociateSensor = "CreateAndAssociateSensor";
            public const string addOrRemovePorduct = "addOrRemovePorduct";
            public const string getList = "List";
        }

        public class CarWebAPI{
            public const string RegisterCar = "RegisterCar";
            public const string AssociateCarToGarage = "AssociateCarToGarage";
            public const string RequestAccessGarage = "RequestAccessGarage";
            public const string CreateAndAssociateSensor = "CreateAndAssociateSensor";
            public const string CreateAndAssociateActuator = "CreateAndAssociateActuator";
            public const string AddSensorReading = "AddSensorReading";
            public const string AddActuatorAction = "AddActuatorAction";
        }

        public class GarageWebAPI
        {
            public const string RegisterGarage = "RegisterGarage";
            public const string AssociateGarageToHome = "AssociateGarageToHome";
            public const string StatusOfGarage = "StatusOfGarage";
            public const string CreateAndAssociateSensor = "CreateAndAssociateSensor";
            public const string CreateAndAssociateActuator = "CreateAndAssociateActuator";
            public const string AddSensorReading = "AddSensorReading";
            public const string AddActuatorAction = "AddActuatorAction";
            public const string GarageDoor = "GarageDoor";
            public const string Request = "Request";
            public const string CheckFire = "Fire";
        }

        public class HouseWebAPI
        {
            public const string RegisterHouse = "RegisterHouse";

            public const string MaxTemperature = "MaxTemperature";
            public const string MinTemperature = "MinTemperature";
            public const string SomeOneInHome = "SomeOneInHome";
            public const string HaveGarage = "HaveGarage";
            public const string Fire = "Fire";

            public const string CreateAndAssociateSensor = "CreateAndAssociateSensor";
            public const string CreateAndAssociateActuator = "CreateAndAssociateActuator";
            public const string AddSensorReading = "AddSensorReading";
            public const string AddActuatorAction = "AddActuatorAction";

        }

        public class CoolerWebAPI
        {
            public const string RegisterCooler = "RegisterCooler";
            public const string Status = "Status";

            public const string AssociateCoolerToHouse = "AssociateCoolerToHouse";
            public const string CheckHouseGarageAssociated = "CheckHouseGarageAssociated";
            public const string someoneHome= "someoneHome";
            public const string coolerPower = "coolerPower";

            public const string CreateAndAssociateSensor = "CreateAndAssociateSensor";
            public const string CreateAndAssociateActuator = "CreateAndAssociateActuator";
            public const string AddSensorReading = "AddSensorReading";
            public const string AddActuatorAction = "AddActuatorAction";
        }
        #region webService Action Name
        public const string RegisterResource = "RegisterResource";
        public const string RegisterDevice = "RegisterDevice";
        public const string Associate = "Associate";
        public const string HouseTemperature = "HouseTemperature";
        public const string GarageAccess = "GarageAccess";
        public const string TemperaturePropertie = "RangeTemperature";
        public const string GaragePropertie = "GaragePropertie";
        public const string DeviceData = "DeviceData";
        public const string DeviceLastData = "DeviceLastData";
        public const string PropertieListCheck = "ListMaker";
        #endregion
    }
}