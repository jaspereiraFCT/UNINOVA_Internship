﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AISCloud.Models.DAL;
using AISCloud.Common;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;

namespace AISCloud.Controllers
{
    public class CoolerController : ApiController
    {
        private AISCloudModelContainer db = new AISCloudModelContainer();

        #region WebAPI

        #region RegisterCooler POST

        /// <summary>
        /// Register a Cooler and 1 default properties(Status) in the cloud
        /// </summary>
        /// <param name="Name">Cooler Name</param>
        /// <returns></returns>
        [ActionName(Dictionary.CoolerWebAPI.RegisterCooler)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterCooler(string Name)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = db.Resource.Add(new Resource { Name = Name, ResourceTypesId = type.Id });

            db.Propertie.Add(new Propertie { ResourceId = cooler.Id, Name = PropertieName.Status, Value = Boolean.FalseString });

            if (await db.SaveChangesAsync() == 2) return Ok(cooler.Id);
            return InternalServerError();
        }
        #endregion

        #region AssociateCoolerToHome

        /// <summary>
        /// Associate Cooler to last registed House
        /// </summary>
        /// <param name="CoolerID"> CoolerID</param>
        /// <returns></returns>
        [ActionName(Dictionary.CoolerWebAPI.AssociateCoolerToHouse)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutAssociateGarageToHome(long CoolerID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is cooler and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = db.Resource.Find(CoolerID);
            if (cooler == null || type.Id != cooler.ResourceTypesId) return BadRequest("Not a Cooler!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource house = db.Resource.Where(b => b.ResourceTypesId == type.Id).OrderByDescending(x => x.Id).FirstOrDefault();

            if (house == null) return Ok(false);

            cooler.ResourceId = house.Id;
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return InternalServerError();
        }

        #endregion

        #region SomeoneHome GET
        /// <summary>
        /// Get if someone is home need the garage
        /// </summary>
        /// <param name="CoolerID">Cooler ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.CoolerWebAPI.someoneHome)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetSomeoneHome(long CoolerID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is cooler and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = db.Resource.Find(CoolerID);
            if (cooler == null || type.Id != cooler.ResourceTypesId) return BadRequest("Not a Cooler!");
            if (cooler.ResourceId == null) return BadRequest("Cooler not associated!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Where(b=> b.ResourceId == cooler.ResourceId && b.ResourceTypesId == type.Id).FirstOrDefault();
            if(garage == null) return BadRequest("Home don t have a garage!");
            Propertie prop = db.Propertie.Where(b => b.ResourceId == garage.Id && b.Name == PropertieName.Status).FirstOrDefault();

            if (prop != null) return Ok(!bool.Parse(prop.Value));
            return InternalServerError();
        }
        #endregion

        #region CoolerPower GET
        /// <summary>
        /// Get Cooler Power based in home max and min temp
        /// </summary>
        /// <param name="CoolerID">Cooler ID</param>
        /// <param name="currentTemperature"> Current House Temperature</param>
        /// <returns>Min-currente/2 +1 or (Currrente-max/2 +1)*-1</returns>
        [ActionName(Dictionary.CoolerWebAPI.coolerPower)]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetCoolerPower(long CoolerID, decimal currentTemperature)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is cooler and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = db.Resource.Find(CoolerID);
            if (cooler == null || type.Id != cooler.ResourceTypesId) return BadRequest("Not a Cooler!");
            if (cooler.ResourceId == null) return BadRequest("Cooler not associated!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.HOME).FirstOrDefault();
            Resource home = db.Resource.Where(b => b.Id == cooler.ResourceId && b.ResourceTypesId == type.Id).FirstOrDefault();
            if (home == null) return BadRequest("Cooler don t have a home!");
            Propertie minProp = db.Propertie.Where(b => b.ResourceId == home.Id && b.Name == PropertieName.MinTemp).FirstOrDefault();
            Propertie maxProp = db.Propertie.Where(b => b.ResourceId == home.Id && b.Name == PropertieName.MaxTemp).FirstOrDefault();

            decimal mprop = decimal.Parse(minProp.Value);
            decimal Mprop = decimal.Parse(maxProp.Value);
            int power = 0;

            if (mprop <= currentTemperature && Mprop >= currentTemperature) return Ok(power);

            if (mprop > currentTemperature)
            {
                return Ok((int)(((mprop - currentTemperature) / 2)+1));
            }
            else
            {
                return Ok((int)((((currentTemperature - Mprop) / 2) + 1) * -1));
            }
        }
        #endregion

        #region CheckHouseGarageAssociated
        /// <summary>
        /// Check if some garage is associated to the house
        /// </summary>
        /// <param name="CoolerID"></param>
        /// <returns></returns>
        [ActionName(Dictionary.CoolerWebAPI.CheckHouseGarageAssociated)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetCheckHouseGarageAssociated(long CoolerID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is cooler and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = db.Resource.Find(CoolerID);
            if (cooler == null || type.Id != cooler.ResourceTypesId) return BadRequest("Not a Cooler!");
            if (cooler.ResourceId == null) return BadRequest("Cooler not associated!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Where(b => b.ResourceId == cooler.ResourceId && b.ResourceTypesId == type.Id).FirstOrDefault();
            if (garage == null) return Ok(false);
            return Ok(true);
        }
        #endregion

        #region Status GET PUT

        /// <summary>
        /// Check Status of Cooler
        /// </summary>
        /// <param name="CoolerID">Cooler ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.CoolerWebAPI.Status)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetStatus(long CoolerID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = await db.Resource.FindAsync(CoolerID);
            if (cooler == null || cooler.ResourceTypesId != type.Id) return BadRequest("You Are Not a Cooler");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == CoolerID && x.Name == PropertieName.Status).OrderByDescending(x => x.Id).FirstOrDefault();

            if (prop == null) return NotFound();
            return Ok(bool.Parse(prop.Value));
        }


        /// <summary>
        /// Change Status of Cooler
        /// </summary>
        /// <param name="CoolerID">Cooler ID</param>
        /// <param name="value"> True if ON and False if OFF</param>
        /// <returns></returns>
        [ActionName(Dictionary.CoolerWebAPI.Status)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutStatus(long CoolerID, bool value)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = await db.Resource.FindAsync(CoolerID);
            if (cooler == null || cooler.ResourceTypesId != type.Id) return BadRequest("You Are Not a Garage");

            Propertie prop = db.Propertie.Where(x => x.ResourceId == CoolerID && x.Name == PropertieName.Status).OrderByDescending(x => x.Id).FirstOrDefault();
            prop.Value = value.ToString();

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #endregion

        #region CreateAndAssociateSensor POST

        /// <summary>
        /// Create and associate a Sensor to the Cooler
        /// </summary>
        /// <param name="CoolerID">Cooler ID</param>
        /// <param name="SensorName"> Name of the Sensor</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.CreateAndAssociateSensor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateSensor(long CoolerID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource resource = db.Resource.Find(CoolerID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Cooler or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = CoolerID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region CreateAndAssociateActuator POST

        /// <summary>
        /// Create and associate a Actuator to the Cooler
        /// </summary>
        /// <param name="CoolerID"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.CreateAndAssociateActuator)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateActuator(long CoolerID, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource resource = db.Resource.Find(CoolerID);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Cooler or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = CoolerID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddSensorReading POST

        /// <summary>
        /// Registe readings from a Cooler sensor
        /// </summary>
        /// <param name="CoolerID">Garage ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.AddSensorReading)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddSensorReading(long CoolerID, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = await db.Resource.FindAsync(CoolerID);
            if (cooler == null || cooler.ResourceTypesId != type.Id) return BadRequest("You are not a Cooler");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == cooler.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddActuatorAction POST

        /// <summary>
        /// Registe readings from a Cooler Actuator
        /// </summary>
        /// <param name="CoolerID">Garage ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.GarageWebAPI.AddActuatorAction)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddActuatorAction(long CoolerID, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.COOLER).FirstOrDefault();
            Resource cooler = await db.Resource.FindAsync(CoolerID);
            if (cooler == null || cooler.ResourceTypesId != type.Id) return BadRequest("You are not a Cooler");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == cooler.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #endregion
    }
}
