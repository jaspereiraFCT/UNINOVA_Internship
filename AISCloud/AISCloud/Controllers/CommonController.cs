﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AISCloud.Models.DAL;
using AISCloud.Common;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;

namespace AISCloud.Controllers
{
    public class CommonController : ApiController
    {

        /// <summary>
        /// My DB
        /// </summary>
        private AISCloudModelContainer db = new AISCloudModelContainer();

        /// <summary>
        /// Register Resources
        /// </summary>
        /// <param name="type">Type of the Resource(Home, Car, Cooler, Freezer and Garage)</param>
        /// <param name="name">The Name of the Resource</param>
        /// <returns>200 Ok: Register (body with id) || 404 NotFound : type don t exist || 400 BadRequest: if not well formated the request || 500 InternalServerError not saved </returns>
        [ActionName(Dictionary.RegisterResource)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterResource(ResourcesTypeName type, string name = "kermit")
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType _type = db.ResourcesType.Where(b => b.Name == type).FirstOrDefault();
            if (_type == null) return NotFound();


            Resource _new = db.Resource.Add(new Resource { Name = name, ResourceTypesId = _type.Id });
            if (await db.SaveChangesAsync() == 1) return Ok(_new.Id);
            return InternalServerError();
        }

        /// <summary>
        /// Register Device
        /// </summary>
        /// <param name="type">Type of the Device(Actuator or Sensor)</param>
        /// <param name="name">The Name of the Device</param>
        /// <returns>200 Ok: Register (body with id) || 404 NotFound : type don t exist || 400 BadRequest: if not well formated the request || 500 InternalServerError not saved </returns>
        [ActionName(Dictionary.RegisterDevice)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterDevice(DevicesTypeName type, string name = "Scooter")
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType _type = db.DeviceType.Where(b => b.Name == type).FirstOrDefault();
            if (_type == null) return NotFound();


            Device _new = db.Device.Add(new Device { Name = name, DeviceTypeId = _type.Id });
            if (await db.SaveChangesAsync() == 1) return Ok(_new.Id);
            return InternalServerError();
        }

        /// <summary>
        /// Associate a Master-Slave relationship
        /// </summary>
        /// <param name="myID">ID of Slave</param>
        /// <param name="myType">Type of Slave</param>
        /// <param name="masterID">ID of Master</param>
        /// <param name="masterType">Type of Master</param>
        /// <returns></returns>
        [ActionName(Dictionary.Associate)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutAssociate(long myID, objectType myType, long masterID, objectType masterType)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (myType == objectType.DEVICE && masterType == objectType.RESOURCE)
            {
                Device me = DeviceExist(myID);
                Resource master = ResourceExist(masterID);

                if (me == null || master == null) return NotFound();
                me.ResourceId = master.Id;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    return Ok(false);
                }

                return Ok(true);
            }

            if (myType == objectType.RESOURCE && masterType == objectType.RESOURCE)
            {
                Resource me = ResourceExist(myID);
                Resource master = ResourceExist(masterID);

                if (me == null || master == null) return NotFound();
                if (me.Id == master.Id) return BadRequest();
                me.ResourceId = master.Id;

                try
                {
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    return Ok(false);
                }

                return Ok(true);
            }

            //if (myType == objectType.RESOURCE && masterType == objectType.DEVICE){}
            //if (myType == objectType.DEVICE && masterType == objectType.DEVICE) { }

            return StatusCode(HttpStatusCode.NotImplemented);
        }

        /// <summary>
        /// Ask access to the Garage
        /// </summary>
        /// <param name="myID">Car ID</param>
        /// <returns>Disponibility of the Garage</returns>
        [ActionName(Dictionary.GarageAccess)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutGarageAccess(long myID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource car = ResourceExist(myID);
            if (car == null || car.ResourceTypesId == null) return NotFound();
            ResourcesType type = db.ResourcesType.Find(car.ResourceTypesId);
            if (type == null || type.Name != ResourcesTypeName.CAR) return BadRequest();

            Resource house = ResourceExist((long)car.ResourceTypesId);
            type = db.ResourcesType.Find(house.ResourceTypesId);
            if (type.Name != ResourcesTypeName.HOME) return BadRequest();

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = ResourceExist(house.Id, type);

            if (garage == null) return BadRequest();

            //falta fazer as properties
            Propertie door = db.Propertie.Where(b => b.ResourceId == garage.Id).FirstOrDefault();
            if (door == null || door.Name != Dictionary.DOOR) return BadRequest();

            bool res = bool.Parse(door.Value);

            if (res)
            {
                door.Value = bool.FalseString;
                await db.SaveChangesAsync();
            }

            return Ok(res);

        }

        /// <summary>
        /// Check if the temperature is in range
        /// </summary>
        /// <param name="myID">House ID</param>
        /// <param name="temperature">Actual Temperature</param>
        /// <returns>-1 - higher, 1 - lower, 0 - inside range</returns>
        [ActionName(Dictionary.HouseTemperature)]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetTemperatureDecision(long myID, decimal temperature)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource house = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(house.ResourceTypesId);
            if (type == null || type.Name != ResourcesTypeName.HOME) return BadRequest();

            Propertie tempMax = db.Propertie.Where(b => (b.ResourceId == house.Id && b.Name == Dictionary.max)).FirstOrDefault();
            Propertie tempMin = db.Propertie.Where(b => (b.ResourceId == house.Id && b.Name == Dictionary.min)).FirstOrDefault();

            if (tempMin == null && tempMax == null) return NotFound();

            if (decimal.Parse(tempMax.Value) < temperature) return Ok(-1);
            if (decimal.Parse(tempMin.Value) > temperature) return Ok(1);
            return Ok(0);


        }


        /// <summary>
        /// Set min or max House Temperature
        /// </summary>
        /// <param name="myID">House Id</param>
        /// <param name="minOrMax"> min = falase, max = true</param>
        /// <param name="value">Value in Celsius</param>
        /// <returns>True or False if Register</returns>
        [ActionName(Dictionary.TemperaturePropertie)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostPropertieTemperatue(long myID, bool minOrMax, decimal value = 22)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource house = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(house.ResourceTypesId);
            if (house == null || type.Name != ResourcesTypeName.HOME) return BadRequest();

            if (minOrMax)
            {
                db.Propertie.Add(new Propertie { Value = value.ToString(), Name = Dictionary.max, ResourceId = myID });
            }
            else
            {
                db.Propertie.Add(new Propertie { Value = value.ToString(), Name = Dictionary.min, ResourceId = myID });
            }

            if (await db.SaveChangesAsync() == 1) return Ok(true);

            return Ok(false);

        }

        /// <summary>
        /// Set Propertie for Garage Space
        /// </summary>
        /// <param name="myID">Garage ID</param>
        /// <param name="value">True = clear, False = occupied</param>
        /// <returns></returns>
        [ActionName(Dictionary.GaragePropertie)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostPropertieDoor(long myID, bool value = true)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource garage = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(garage.ResourceTypesId);
            if (garage == null || type.Name != ResourcesTypeName.GARAGE) return BadRequest();

            db.Propertie.Add(new Propertie { Value = value.ToString(), Name = Dictionary.DOOR, ResourceId = myID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);

            return Ok(false);
        }


        /// <summary>
        /// Change Propertie for Garage Space
        /// </summary>
        /// <param name="myID">Home ID</param>
        /// <param name="value">True = clear, False = occupied</param>
        /// <returns></returns>
        [ActionName(Dictionary.GaragePropertie)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutPropertieDoor(long myID, bool value = true)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource home = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(home.ResourceTypesId);
            if (home == null || type.Name != ResourcesTypeName.HOME) return BadRequest();

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = ResourceExist(home.Id, type);

            if (garage == null) return BadRequest();

            Propertie aux = db.Propertie.Where(b => (b.ResourceId == garage.Id && b.Name == Dictionary.DOOR)).FirstOrDefault();
            if (aux == null) return BadRequest();

            aux.Value = value.ToString();

            if (await db.SaveChangesAsync() == 1) return Ok(true);

            return Ok(false);
        }



        /// <summary>
        /// Set Propertie Home List Request
        /// </summary>
        /// <param name="myID">Home ID</param>
        /// <param name="value">True = requested, False = not requested</param>
        /// <returns></returns>
        [ActionName(Dictionary.PropertieListCheck)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostPropertieList(long myID, bool value = false)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource home = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(home.ResourceTypesId);
            if (home == null || type.Name != ResourcesTypeName.HOME) return BadRequest();

            db.Propertie.Add(new Propertie { Value = value.ToString(), Name = Dictionary.list, ResourceId = myID });

            if (await db.SaveChangesAsync() == 1) return Ok(true);

            return Ok(false);
        }

        /// <summary>
        /// Get Value of Home List Request
        /// </summary>
        /// <param name="myID">Home ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.PropertieListCheck)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetPropertieListCheck(long myID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource home = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(home.ResourceTypesId);
            if (home == null || type.Name != ResourcesTypeName.HOME) return BadRequest();

            IEnumerable<Propertie> res = db.Propertie.Where(b => (b.ResourceId == myID && b.Name == Dictionary.list)).OrderByDescending(x => x.Id);
            if (res.Count() > 0) return Ok(res.First<Propertie>().Value);
            return NotFound();
        }

        /// <summary>
        /// Put Propertie List Check
        /// </summary>
        /// <param name="myID">Home Id</param>
        /// <param name="isRequest">New Request: true or false</param>
        /// <returns></returns>
        [ActionName(Dictionary.PropertieListCheck)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutPropertieListCheck(long myID, bool isRequest)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Resource home = db.Resource.Find(myID);
            ResourcesType type = db.ResourcesType.Find(home.ResourceTypesId);
            if (home == null || type.Name != ResourcesTypeName.HOME) return BadRequest();

            IEnumerable<Propertie> res = db.Propertie.Where(b => (b.ResourceId == myID && b.Name == Dictionary.list)).OrderByDescending(x => x.Id);
            res.First<Propertie>().Value = isRequest.ToString();
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        /// <summary>
        /// List of All Data from the device with that nameProp
        /// </summary>
        /// <param name="myID">Device ID</param>
        /// <param name="nameProp">Propertie Name</param>
        /// <returns>List Of Data</returns>
        [ActionName(Dictionary.DeviceData)]
        [ResponseType(typeof(IEnumerable<Data>))]
        public async Task<IHttpActionResult> GetDeviceData(long myID, string nameProp)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Device dev = DeviceExist(myID);

            if (dev == null) return BadRequest();

            IEnumerable<Data> res = db.Data.Where(b => (b.DeviceId == myID && b.NameProp == nameProp));
            if (res.Count() > 0) return Ok(res);
            return NotFound();
        }

        /// <summary>
        /// Last Data from the device with that nameProp
        /// </summary>
        /// <param name="myID">>Device ID</param>
        /// <param name="nameProp">Propertie Name</param>
        /// <returns>Last Data</returns>
        [ActionName(Dictionary.DeviceLastData)]
        [ResponseType(typeof(Data))]
        public async Task<IHttpActionResult> GetDeviceLastData(long myID, string nameProp)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Device dev = DeviceExist(myID);

            if (dev == null) return BadRequest();

            IEnumerable<Data> res = db.Data.Where(b => (b.DeviceId == myID && b.NameProp == nameProp)).OrderByDescending(x => x.Date);
            if (res.Count() > 0) return Ok(res.First<Data>());
            return NotFound();
        }

        /// <summary>
        /// Register Device Data
        /// </summary>
        /// <param name="myID">Device ID</param>
        /// <param name="NameProp">Propertie Name</param>
        /// <param name="value">Value of Propertie</param>
        /// <param name="tEn">Data Type of Propertie</param>
        /// <returns>True or False if Register</returns>
        [ActionName(Dictionary.DeviceData)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostDeviceData(long myID, string NameProp, string value, TypeEnumName tEn)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            Device dev = DeviceExist(myID);
            if (dev == null) return BadRequest();

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = myID, NameProp = NameProp, typeEnum = tEn, value = value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }

        #region Private Functions
        private Resource ResourceExist(long id)
        {
            return db.Resource.Find(id);
        }

        private Resource ResourceExist(long id, ResourcesType type)
        {
            return db.Resource.Where(b => (b.ResourceId == id && b.ResourceTypesId == type.Id)).FirstOrDefault();
        }

        private Device DeviceExist(long id)
        {
            return db.Device.Find(id);
        }

        #endregion


    }
}
