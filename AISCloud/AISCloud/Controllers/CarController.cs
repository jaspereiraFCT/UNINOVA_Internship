﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using AISCloud.Models.DAL;
using AISCloud.Common;
using System.Threading.Tasks;
using System.Web.Http.Description;
using System.Data.Entity.Infrastructure;

namespace AISCloud.Controllers
{
    public class CarController : ApiController
    {
        private AISCloudModelContainer db = new AISCloudModelContainer();

        #region WebAPI

        #region RegisterCar POST
        /// <summary>
        /// Register one car in the cloud
        /// </summary>
        /// <param name="Name">Car Name</param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.RegisterCar)]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostRegisterCar(string Name)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource car = db.Resource.Add(new Resource { Name = Name, ResourceTypesId = type.Id });
            if (await db.SaveChangesAsync() == 1) return Ok(car.Id);
            return InternalServerError();
        }

        #endregion

        #region AssociateCarToGarage PUT

        /// <summary>
        /// Associate Car to last registed House
        /// </summary>
        /// <param name="CarID"> Car ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.AssociateCarToGarage)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PutAssociateCarToHouse(long CarID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is car and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource car = db.Resource.Find(CarID);
            if (car == null || type.Id != car.ResourceTypesId) return BadRequest("Not a Car!");

            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource garage = db.Resource.Where(b => b.ResourceTypesId == type.Id).OrderByDescending(x => x.Id).FirstOrDefault();

            if (garage == null) return Ok(false);

            car.ResourceId = garage.Id;
            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return InternalServerError();
        }

        #endregion

        #region RequestAccessGarage GET
        /// <summary>
        /// Request Access to know if state of garage that i am associate is free or not
        /// </summary>
        /// <param name="CarID">Car ID</param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.RequestAccessGarage)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> GetRequestAccessGarage(long CarID)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            //check if is car and is register
            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource car = db.Resource.Find(CarID);
            if (car == null || type.Id != car.ResourceTypesId) return BadRequest("Not a Car!");
            if (car.ResourceId == null) return BadRequest("Not Associate to Any Resource!");

            //Check any Garage associated to the car
            type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.GARAGE).FirstOrDefault();
            Resource Garage = db.Resource.Where(b => b.Id == car.ResourceId && type.Id == b.ResourceTypesId).OrderByDescending(x => x.Id).FirstOrDefault();
            if (Garage == null) return BadRequest("Car don t have any Garage!");

            Propertie garageProp = db.Propertie.Where(b => b.ResourceId == Garage.Id && b.Name == PropertieName.Requested).OrderByDescending(x => x.Id).FirstOrDefault();
            garageProp.Value = bool.TrueString;
            await db.SaveChangesAsync();

            //See Garage Propertie
            Propertie prop = db.Propertie.Where(b => b.ResourceId == (long)Garage.Id && b.Name == PropertieName.Status).OrderByDescending(x => x.Id).FirstOrDefault();
            if (prop == null) return BadRequest("Propertie don t exist");
            return Ok(bool.Parse(prop.Value));
        }
        #endregion

        #region CreateAndAssociateSensor POST

        /// <summary>
        /// Create and associate a Sensor to the car
        /// </summary>
        /// <param name="CarId"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.CreateAndAssociateSensor)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateSensor(long CarId, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource resource = db.Resource.Find(CarId);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Car or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id , ResourceId = CarId});

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region CreateAndAssociateActuator POST

        /// <summary>
        /// Create and associate a Actuator to the car
        /// </summary>
        /// <param name="CarId"></param>
        /// <param name="SensorName"></param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.CreateAndAssociateActuator)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostCreateAndAssociateActuator(long CarId, string SensorName)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            DeviceType type = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            ResourcesType resType = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource resource = db.Resource.Find(CarId);
            if (resource == null || resource.ResourceTypesId != resType.Id) return BadRequest("You are not a Car or you are not register");

            Device sensor = db.Device.Add(new Device { Name = SensorName, DeviceTypeId = type.Id, ResourceId = CarId });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddSensorReading POST

        /// <summary>
        /// Registe readings from a car sensor
        /// </summary>
        /// <param name="CarId">Car ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.AddSensorReading)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddSensorReading(long CarId, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource car = await db.Resource.FindAsync(CarId);
            if (car == null || car.ResourceTypesId != type.Id) return BadRequest("You are not a car");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.SENSOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == car.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value= Value});

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #region AddActuatorAction POST

        /// <summary>
        /// Registe readings from a car Actuator
        /// </summary>
        /// <param name="CarId">Car ID</param>
        /// <param name="SensorName">Sensor Name</param>
        /// <param name="DataName">Name of the reading propertie</param>
        /// <param name="Value">Value of the reading</param>
        /// <param name="TypeObject">Type of data of the reading</param>
        /// <returns></returns>
        [ActionName(Dictionary.CarWebAPI.AddActuatorAction)]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> PostAddActuatorAction(long CarId, string SensorName, DataName DataName, string Value, TypeEnumName TypeObject)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            ResourcesType type = db.ResourcesType.Where(b => b.Name == ResourcesTypeName.CAR).FirstOrDefault();
            Resource car = await db.Resource.FindAsync(CarId);
            if (car == null || car.ResourceTypesId != type.Id) return BadRequest("You are not a car");

            DeviceType devType = db.DeviceType.Where(b => b.Name == DevicesTypeName.ACTUADOR).FirstOrDefault();
            Device dev = db.Device.Where(b => b.Name.ToLower() == SensorName.ToLower() && b.ResourceId == car.Id && b.DeviceTypeId == devType.Id).FirstOrDefault();

            if (dev == null) return BadRequest("Sensor don t exist or no associate o wrong type");

            db.Data.Add(new Data { Date = DateTime.Now, DeviceId = dev.Id, NameProp = DataName, typeEnum = TypeObject, value = Value });

            if (await db.SaveChangesAsync() == 1) return Ok(true);
            return Ok(false);
        }
        #endregion

        #endregion

    }
}
