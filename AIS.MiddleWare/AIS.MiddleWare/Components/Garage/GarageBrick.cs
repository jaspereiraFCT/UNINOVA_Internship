﻿using AIS.MiddleWare.Model;
using System;
using System.Threading.Tasks;
using System.Threading;

namespace AIS.MiddleWare.Components.Garage
{
    public class GarageBrick : AISMiddleWare
    {
        private GarageAPI _API = null;
        private BrickModel _myGarage = null;
        public GarageBrick(string ComPort, BrickModel myGarage)
        {
            if (myGarage == null) throw new ArgumentNullException("myGarage");
            this._myGarage = myGarage;
            this._myGarage.ID = -1;
            this._API = new GarageAPI();
            Console.WriteLine("{0} - Start Config and register.", this._myGarage.Name);
            if (!BuildAndRegisterBrick(ComPort).Result) throw new Exception("Brick Not Connected");
            Console.WriteLine("{0} - End Config and register.", this._myGarage.Name);
        }
        public void GarageWork()
        {
            bool printAssociate = true;
            int print = 0;
            bool fireDoor = true;
            Console.WriteLine("{0} - Start Working.", this._myGarage.Name);

            while (!_API.PutAssociateAsync(this._myGarage).Result)
            {
                if (printAssociate)
                {
                    Console.WriteLine("{0} - Trying to Associate", this._myGarage.Name);
                    printAssociate = false;
                }
            };

            Console.WriteLine("{0} - Associated", this._myGarage.Name);

            while (true)
            {

                while(_API.GetFireAsync(_myGarage).Result){
                    if (!_API.GetStatusOfGarageAsync(_myGarage).Result)
                    {
                        if (fireDoor)
                        {
                            while (!OpenDoor().Result) ;//Open Door
                            Console.WriteLine("{0} - Door Open", this._myGarage.Name);
                            while (!FrontSonar().Result) ;
                            Console.WriteLine("{0} - Car Outside detected", this._myGarage.Name);
                            Thread.Sleep(2000);
                            Console.WriteLine("{0} - Car Outside Final", this._myGarage.Name);
                            while (!CloseDoor().Result) ; //close Door
                            Console.WriteLine("{0} - Door Closed", this._myGarage.Name);
                            if (_API.PutStatusOfGarageAsync(_myGarage, true).Result) //end request
                            {
                                Console.WriteLine("{0} - Car Out", this._myGarage.Name);
                            }
                            fireDoor = false;
                        }
                    }
                }

                fireDoor = true;

                if (_API.GetRequestAsync(_myGarage).Result) //tem pedido??
                {
                    if (print == 0)
                    {
                        Console.WriteLine("{0} - Got a Request.", this._myGarage.Name);
                        print++;
                    }

                    //Verifica se e para entrar ou sair
                    if (_API.GetStatusOfGarageAsync(_myGarage).Result)
                    {
                        if (print == 1)
                        {
                            Console.WriteLine("{0} - Garage Clear", this._myGarage.Name);
                            print++;
                        }

                        if (FrontSonar().Result && !_API.GetFireAsync(_myGarage).Result)
                        { //check front sonar until car inside
                            Console.WriteLine("{0} - Car Outside detected", this._myGarage.Name);
                            while (!OpenDoor().Result) ;//Open Door
                            Console.WriteLine("{0} - Door Open", this._myGarage.Name);
                            while (!BackSonar().Result) ; //check Back sonar until car inside
                            Console.WriteLine("{0} - Car Inside detected", this._myGarage.Name);
                            while (!CloseDoor().Result) ; //close Door
                            Thread.Sleep(2000);
                            Console.WriteLine("{0} - Door Closed", this._myGarage.Name);
                            if (_API.PutRequestAsync(_myGarage, false).Result && _API.PutStatusOfGarageAsync(_myGarage, false).Result) //end request
                            {
                                Console.WriteLine("{0} - Resquest Terminated", this._myGarage.Name);
                            }
                        }

                    }
                    else
                    {
                        if (print == 1)
                        {
                            Console.WriteLine("{0} - Car is inside", this._myGarage.Name);
                            print++;
                        }

                        while (!OpenDoor().Result) ;//Open Door
                        Console.WriteLine("{0} - Door Open", this._myGarage.Name);
                        while (!FrontSonar().Result) ;
                        Console.WriteLine("{0} - Car Outside detected", this._myGarage.Name);
                        Thread.Sleep(2000);
                        Console.WriteLine("{0} - Car Outside Final", this._myGarage.Name);
                        while (!CloseDoor().Result) ; //close Door
                        Console.WriteLine("{0} - Door Closed", this._myGarage.Name);
                        if (_API.PutRequestAsync(_myGarage, false).Result && _API.PutStatusOfGarageAsync(_myGarage, true).Result) //end request
                        {
                            Console.WriteLine("{0} - Resquest Terminated", this._myGarage.Name);
                        }
                    }

                }
                else
                {
                    print = 0;
                }

            }

        }

        #region Private
        private async Task<bool> BuildAndRegisterBrick(string ComPost)
        {
            try
            {

                bool res = ConnectBrick(ComPost);
                if (!res) return false;

                this._myGarage.ID = await _API.PostRegisterAsync(this._myGarage);
                if (this._myGarage.ID == -1)
                {
                    DisconnectBrick();
                    return false;
                }

                AssociateSensors(1, MySensorType.Sonar);//sensor de frente
                AssociateSensors(2, MySensorType.Sonar); //sensor de tras
                AssociateSensors(3, MySensorType.Touch); //sensor de tras
                NxtBrick.MotorA.Reverse = true;
                Task<bool> res1 = _API.PostCreateAndAssociateActuatorAsync(this._myGarage, this._myGarage.Motor1);
                Task<bool> res2 = _API.PostCreateAndAssociateSensorAsync(this._myGarage, this._myGarage.Sensor1);
                Task<bool> res3 = _API.PostCreateAndAssociateSensorAsync(this._myGarage, this._myGarage.Sensor2);

                if (!await res1 || !await res2 || !await res3) return false;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async Task<bool> FrontSonar()
        {
            try
            {
                string[] reading = NxtBrick.Sensor1.ReadAsString().Split(' ');
                string[] reading2 = NxtBrick.Sensor2.ReadAsString().Split(' ');
                int distancia = Int32.Parse(reading[0]);
                int distancia2 = Int32.Parse(reading2[0]);
                _myGarage.Sensor1.Value = distancia.ToString();
                if (await _API.PostAddSensorReadingAsync(_myGarage, _myGarage.Sensor1))
                {
                    Console.WriteLine("{0} Sensor {1} - Register a reading {2} e {3} (cm).", _myGarage.Name, _myGarage.Sensor1.Name, distancia,distancia2);
                }
                else
                {
                    Console.WriteLine("{0} Sensor {1} - Error register a reading {2} e {3}  (cm).", _myGarage.Name, _myGarage.Sensor1.Name, distancia, distancia2);
                }
                if (distancia < 25 && distancia2 < 30) return true;
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        private async Task<bool> BackSonar()
        {
            try
            {
                string reading = NxtBrick.Sensor3.ReadAsString();
                bool res = false;

                if (reading != "0") res = true;
                _myGarage.Sensor2.Value = res.ToString();

                if (await _API.PostAddSensorReadingAsync(_myGarage, _myGarage.Sensor2))
                {
                    Console.WriteLine("{0} Sensor {1} - Register a reading.", _myGarage.Name, _myGarage.Sensor2.Name, res);
                }
                else
                {
                    Console.WriteLine("{0} Sensor {1} - Error register a reading .", _myGarage.Name, _myGarage.Sensor2.Name, res);
                }
                return res;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private async Task<bool> OpenDoor()
        {
            if (!await _API.GetGarageDoorAsync(_myGarage))
            {
                if (await _API.PutGarageDoorAsync(_myGarage, true))
                {
                    NxtBrick.MotorA.Reverse = true; //true enrola
                    NxtBrick.MotorA.On(30, 620); //640 true //600 false
                    _myGarage.Motor1.Value = "30";
                    await _API.PostAddActuatorActionAsync(_myGarage, _myGarage.Motor1);
                    await _API.PutGarageDoorAsync(_myGarage, true);
                    Console.WriteLine("{0} - Door status change to  Open.", this._myGarage.Name);
                    return true;
                }
                else
                {
                    Console.WriteLine("{0} -  ERROR changing Door status.", this._myGarage.Name);
                    return false;
                }

            }
            else
            {
                Console.WriteLine("{0} - Door Already Open.", this._myGarage.Name);
                return false;
            }

        }
        private async Task<bool> CloseDoor()
        {
            if (await _API.GetGarageDoorAsync(_myGarage))
            {
                if (await _API.PutGarageDoorAsync(_myGarage, false))
                {
                    NxtBrick.MotorA.Reverse = false; //true enrola
                    NxtBrick.MotorA.On(30, 620); //640 true //600 false
                    _myGarage.Motor1.Value = "-30";
                    await _API.PostAddActuatorActionAsync(_myGarage, _myGarage.Motor1);
                    await _API.PutGarageDoorAsync(_myGarage, false);
                    Console.WriteLine("{0} - Door status change to  Close.", this._myGarage.Name);
                    return true;
                }
                else
                {
                    Console.WriteLine("{0} -  ERROR changing Door status.", this._myGarage.Name);
                    return false;
                }

            }
            else
            {
                Console.WriteLine("{0} - Door Already Close.", this._myGarage.Name);
                return false;
            }
        }
        #endregion
    }
}
