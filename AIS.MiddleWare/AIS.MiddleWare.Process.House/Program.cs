﻿using AIS.MiddleWare.Components.House;
using AIS.MiddleWare.Model;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AIS.MiddleWare.Process.House
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /*args = new string[3];
                args[1] = "cenas1";
                args[0] = "com6";*/
                Console.WriteLine("Name: {0} Bluetooth Port: {1}", args[1], args[0]);
                HouseBrick houseBrick = new HouseBrick(args[0], new BrickModel
                {
                    ID = -1,
                    Name = args[1],
                    Motor1 = new DeviceModel { DataName = DataName.Velocidade, DataType = TypeEnumName.@int, Value = "0", Name = "Luz A", SensorType = MySensorType.Motor },
                    Motor2 = new DeviceModel { DataName = DataName.Velocidade, DataType = TypeEnumName.@int, Value = "0", Name = "Luz B", SensorType = MySensorType.Motor },
                    Motor3 = new DeviceModel { DataName = DataName.Velocidade, DataType = TypeEnumName.@int, Value = "0", Name = "Luz C", SensorType = MySensorType.Motor },
                    Sensor1 = new DeviceModel { DataName = DataName.Pressao, DataType = TypeEnumName.@int, Value = "false", Name = "Fire ON", SensorType = MySensorType.Touch },
                    Sensor2 = new DeviceModel { DataName = DataName.Pressao, DataType = TypeEnumName.@int, Value = "false", Name = "Fire OFF", SensorType = MySensorType.Touch }
                });
                houseBrick.HouseWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR : " + ex.Message);
                Console.WriteLine("Press any Button to Exit!");
                Console.ReadKey();
                return;
            }
        }
    }
}
