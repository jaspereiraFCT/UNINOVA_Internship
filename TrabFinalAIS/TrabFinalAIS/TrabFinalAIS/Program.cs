﻿using System;
using System.Threading;
using System.Timers;
using MonoBrick.NXT;
using MonoBrick.Arduino;
namespace Application
{
    public static class Program
    {
        static System.Timers.Timer aTimer;
        static ArduinoChip serialArduino;
        static byte TempMeasured = 0x00;
        static string ArduinoReceived;

        static void Main(string[] args)
        {
            ////// LET THE GAME BEGIN.... 

            //// CONFIG ARDUINO SERIAL
            serialArduino = new MonoBrick.Arduino.ArduinoChip("COM9", 9600);
            Console.WriteLine("Arduino Connected..");
            string UsernameAC = "AC_FILIPE";
            string TypeNXT = "HOME";
            int MinTemp = 27;
            int MaxTemp = 32;

            ArduinoReceived = serialArduino.SerialRead();
            if (string.Compare(ArduinoReceived, "START\r") == 0)
            {
                serialArduino.SerialWrite(UsernameAC);
                Console.WriteLine("REGISTER USERNAME (PC): " + UsernameAC);
                ArduinoReceived = serialArduino.SerialRead();
                Console.WriteLine("MENSAGE RECEIVED (ARDUINO): " + ArduinoReceived);

                if (string.Compare(ArduinoReceived, "USEROK\r") == 0)
                {
                    serialArduino.SerialWrite(TypeNXT);
                    Console.WriteLine("REGISTER NXT TYPE (PC): " + TypeNXT);
                    ArduinoReceived = serialArduino.SerialRead();
                    Console.WriteLine("MENSAGE RECEIVED (ARDUINO): " + ArduinoReceived);

                    if (string.Compare(ArduinoReceived, "TYPEOK\r") == 0)
                    {
                        serialArduino.SerialWrite(MinTemp.ToString());
                        Console.WriteLine("SENDING MIN TEMP (PC): " + MinTemp.ToString());
                        ArduinoReceived = serialArduino.SerialRead();
                        Console.WriteLine("MENSAGE RECEIVED (ARDUINO): " + ArduinoReceived);
                        serialArduino.SerialWrite(MaxTemp.ToString());
                        Console.WriteLine("SENDING MAX TEMP (PC): " + MaxTemp.ToString());
                        ArduinoReceived = serialArduino.SerialRead();
                        Console.WriteLine("MENSAGE RECEIVED (ARDUINO): " + ArduinoReceived);
                        ArduinoReceived = serialArduino.SerialRead();
                        Console.WriteLine("MENSAGE RECEIVED (ARDUINO): " + ArduinoReceived);

                        if (string.Compare(ArduinoReceived, "TEMPOK\r") == 0)
                        {
                            Console.WriteLine("SUCCESSFUL..");
                            Console.WriteLine("USERNAME --> " + UsernameAC);
                            Console.WriteLine("USERNAME TYPE --> " + TypeNXT);
                            Console.WriteLine("TEMPERATURE RANGE -->" + MinTemp.ToString() + " TO " + MaxTemp.ToString());
                        }
                        else
                        {
                            Console.WriteLine("ERROR GETTING TEMPERATUREOK RESPONSE.. RESTART DEVICE");
                        }
                    }
                    else
                    {
                        Console.WriteLine("ERROR GETTING TYPEOK RESPONSE.. RESTART DEVICE");
                    }



                    //// CONFIG BLETOOTH NXT
                    var brick = new Brick<Sensor, Sensor, Sensor, Sensor>("COM6"); //usb  
                    sbyte speed = 0;
                    brick.Connection.Open();
                    I2CSensor TempSensor = new I2CSensor(I2CMode.LowSpeed, 0x98);
                    brick.Sensor1 = TempSensor;

                    aTimer = new System.Timers.Timer(5000);
                    aTimer.Elapsed += myTimer;
                    aTimer.Enabled = true;


                    ConsoleKeyInfo cki;
                    Console.WriteLine("Press Q to quit");
                    do
                    {

                        TempMeasured = TempSensor.ReadRegister(0x00, 1)[0];

                        if (Convert.ToInt32(TempMeasured) > MaxTemp || Convert.ToInt32(TempMeasured) < MinTemp)
                        {
                            speed = 100;
                            brick.MotorA.On(speed);
                        }
                        else if (Convert.ToInt32(TempMeasured) < MaxTemp && Convert.ToInt32(TempMeasured) > MinTemp)
                        {
                            brick.MotorA.Off();
                        }

                        //if(Console.ReadKey(true)
                        //cki = Console.ReadKey(true); //press a key  
                    } while (true);//|| cki.Key != ConsoleKey.Q);

                }
                else
                {
                    Console.WriteLine("ERROR GETTING USEROK RESPONSE.. RESTART DEVICE");
                }
            }

            else
            {
                Console.WriteLine("ERROR GETTING START RESPONSE.. RESTART DEVICE");
            }
        }

        static void myTimer(Object source, ElapsedEventArgs e)
        {
            serialArduino.SerialWrite(TempMeasured.ToString());
            Console.WriteLine("SENDING TEMP MEASURED (PC): " + TempMeasured.ToString());
            ArduinoReceived = serialArduino.SerialRead();
            Console.WriteLine("MENSAGE RECEIVED (ARDUINO): " + ArduinoReceived);
        }
    }
}