﻿using System;
using System.IO.Ports;


namespace MonoBrick.Arduino
{
    /// <summary>
    /// Serial used for Arduino mensaging
    /// </summary>
    public class ArduinoChip
    {
        string COMPort; //Serial Port where Arduino is connected
        int BaudRate = 0;      //Baud Rate
        bool connected = false; //Connection status


        public ArduinoChip(string Port, int Rate)
        {
            serialArduino = new System.IO.Ports.SerialPort(Port, Rate);
            serialArduino.Open();
        }

        public void SerialWrite(string mensage)
        {
            serialArduino.WriteLine(mensage);
        }

        public string SerialRead()
        {
            return serialArduino.ReadLine();
        }

        public System.IO.Ports.SerialPort serialArduino;
    }
}
